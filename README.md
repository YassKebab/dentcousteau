# dentcousteau

This app was bootstrapped with [Symfony5](https://github.com/symfony/symfony).

## Requirements

- PHP >= 7.4
- composer
- postgresql >= 12.2

## Installation and configuration

### Install

```bash
$ composer install
```

### Database implementation

```bash
 # Create database
$ bin/console doctrine:database:create

 # Create or update schema
$ bin/console doctrine:migrations:migrate

 # Load initial data
$ bin/console doctrine:fixtures:load
```

### Launch the webserver (local only)

```bash
$ php -S 127.0.0.1:8000 -t public 
```
