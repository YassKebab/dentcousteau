<?php

namespace App\Form;

use App\Entity\ProductType as ProductTypeEntity;
use App\Entity\ProductTypeProject;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductTypeProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('productType', null, [
                'class' => ProductTypeEntity::class,
                'choice_label' => 'lib',
                'disabled' => true,
                'label' => false
            ])
            ->add('nb', NumberType::class, [
                'label' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductTypeProject::class,
        ]);
    }
}
