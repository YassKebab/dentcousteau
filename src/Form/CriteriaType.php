<?php

namespace App\Form;

use App\Entity\ProductCriteria;
use Symfony\Component\Form\AbstractType;
use App\Entity\Criteria;
use Doctrine\DBAL\Types\TextType as TypesTextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CriteriaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('criteria', null, [
                'class' => Criteria::class,
                'choice_label' => 'lib',
                'disabled' => true,
                'label' => false
            ])
            ->add('value', TextType::class)
            ->add('numeric_value', NumberType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductCriteria::class,
        ]);
    }
}
