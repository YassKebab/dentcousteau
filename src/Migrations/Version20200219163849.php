<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200219163849 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE criteria_product_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE criteria_product_type (id INT NOT NULL, product_type_id INT DEFAULT NULL, criteria_id INT DEFAULT NULL, is_beneficial BOOLEAN NOT NULL, weight DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_853A06D214959723 ON criteria_product_type (product_type_id)');
        $this->addSql('CREATE INDEX IDX_853A06D2990BEA15 ON criteria_product_type (criteria_id)');
        $this->addSql('ALTER TABLE criteria_product_type ADD CONSTRAINT FK_853A06D214959723 FOREIGN KEY (product_type_id) REFERENCES productType (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE criteria_product_type ADD CONSTRAINT FK_853A06D2990BEA15 FOREIGN KEY (criteria_id) REFERENCES criteria (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_criteria DROP is_beneficial');
        $this->addSql('ALTER TABLE product_criteria DROP weight');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE criteria_product_type_id_seq CASCADE');
        $this->addSql('DROP TABLE criteria_product_type');
        $this->addSql('ALTER TABLE product_criteria ADD is_beneficial BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE product_criteria ADD weight DOUBLE PRECISION NOT NULL');
    }
}
