<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200218132503 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE product_criteria_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE project_product_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE criteria_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE productType_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE project_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE product_criteria (id INT NOT NULL, product_id INT DEFAULT NULL, criteria_id INT DEFAULT NULL, value VARCHAR(255) NOT NULL, numeric_value DOUBLE PRECISION NOT NULL, is_beneficial BOOLEAN NOT NULL, weight DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7DAFF1164584665A ON product_criteria (product_id)');
        $this->addSql('CREATE INDEX IDX_7DAFF116990BEA15 ON product_criteria (criteria_id)');
        $this->addSql('CREATE TABLE project_product_type (id INT NOT NULL, project_id INT DEFAULT NULL, product_type_id INT DEFAULT NULL, nb INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2B9191EB166D1F9C ON project_product_type (project_id)');
        $this->addSql('CREATE INDEX IDX_2B9191EB14959723 ON project_product_type (product_type_id)');
        $this->addSql('CREATE TABLE criteria (id INT NOT NULL, lib VARCHAR(255) NOT NULL, unit VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE productType (id INT NOT NULL, lib VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE project (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE project_product (project_id INT NOT NULL, product_id INT NOT NULL, PRIMARY KEY(project_id, product_id))');
        $this->addSql('CREATE INDEX IDX_455408C8166D1F9C ON project_product (project_id)');
        $this->addSql('CREATE INDEX IDX_455408C84584665A ON project_product (product_id)');
        $this->addSql('CREATE TABLE product (id INT NOT NULL, product_type_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D34A04AD14959723 ON product (product_type_id)');
        $this->addSql('CREATE TABLE product_project (product_id INT NOT NULL, project_id INT NOT NULL, PRIMARY KEY(product_id, project_id))');
        $this->addSql('CREATE INDEX IDX_FE6ACB5E4584665A ON product_project (product_id)');
        $this->addSql('CREATE INDEX IDX_FE6ACB5E166D1F9C ON product_project (project_id)');
        $this->addSql('ALTER TABLE product_criteria ADD CONSTRAINT FK_7DAFF1164584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_criteria ADD CONSTRAINT FK_7DAFF116990BEA15 FOREIGN KEY (criteria_id) REFERENCES criteria (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE project_product_type ADD CONSTRAINT FK_2B9191EB166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE project_product_type ADD CONSTRAINT FK_2B9191EB14959723 FOREIGN KEY (product_type_id) REFERENCES productType (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE project_product ADD CONSTRAINT FK_455408C8166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE project_product ADD CONSTRAINT FK_455408C84584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD14959723 FOREIGN KEY (product_type_id) REFERENCES productType (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_project ADD CONSTRAINT FK_FE6ACB5E4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_project ADD CONSTRAINT FK_FE6ACB5E166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE product_criteria DROP CONSTRAINT FK_7DAFF116990BEA15');
        $this->addSql('ALTER TABLE project_product_type DROP CONSTRAINT FK_2B9191EB14959723');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04AD14959723');
        $this->addSql('ALTER TABLE project_product_type DROP CONSTRAINT FK_2B9191EB166D1F9C');
        $this->addSql('ALTER TABLE project_product DROP CONSTRAINT FK_455408C8166D1F9C');
        $this->addSql('ALTER TABLE product_project DROP CONSTRAINT FK_FE6ACB5E166D1F9C');
        $this->addSql('ALTER TABLE product_criteria DROP CONSTRAINT FK_7DAFF1164584665A');
        $this->addSql('ALTER TABLE project_product DROP CONSTRAINT FK_455408C84584665A');
        $this->addSql('ALTER TABLE product_project DROP CONSTRAINT FK_FE6ACB5E4584665A');
        $this->addSql('DROP SEQUENCE product_criteria_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE project_product_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE criteria_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE productType_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE project_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_id_seq CASCADE');
        $this->addSql('DROP TABLE product_criteria');
        $this->addSql('DROP TABLE project_product_type');
        $this->addSql('DROP TABLE criteria');
        $this->addSql('DROP TABLE productType');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE project_product');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_project');
    }
}
