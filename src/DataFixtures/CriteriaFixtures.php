<?php

namespace App\DataFixtures;

use App\Entity\Criteria;
use App\Entity\CriteriaProductType;
use App\Entity\ProductType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CriteriaFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $productTypes = $manager
            ->getRepository(ProductType::class)
            ->findAll();

        foreach ($this->getCriteriaArray() as $criteriaRow) {
            $criteria = new Criteria;
            $criteria->setLib($criteriaRow['lib']);
            $criteria->setUnit($criteriaRow['unit']);

            foreach ($criteriaRow['product_types'] as $cProductType) {
                $criteriaProductType = new CriteriaProductType;
                $criteriaProductType->setWeight($cProductType['weight']);
                $criteriaProductType->setIsBeneficial($cProductType['isBeneficial']);
                $criteriaProductType->setCriteria($criteria);

                foreach ($productTypes as $productType) {
                    if ($cProductType['type'] !== $productType->getLib()) {
                        continue;
                    }
                    $criteriaProductType->setProductType($productType);
                    break;
                }
                $manager->persist($criteriaProductType);
            }
            $manager->persist($criteria);
        }

        $manager->flush();
    }

    protected function getCriteriaArray(): array
    {
        return [
            [
                'lib'  => 'Price',
                'unit' => 'EUR',
                'product_types' => [
                    [
                        'type'         => 'Thermostatic valves',
                        'isBeneficial' => false,
                        'weight'       => .5,
                    ],
                    [
                        'type'         => 'Radiators',
                        'isBeneficial' => false,
                        'weight'       => .4,
                    ],
                    [
                        'type'         => 'Pipes',
                        'isBeneficial' => false,
                        'weight'       => .35,
                    ],
                    [
                        'type'         => 'Balancing valves',
                        'isBeneficial' => false,
                        'weight'       => .5,
                    ],
                    [
                        'type'         => 'Circulation pump',
                        'isBeneficial' => false,
                        'weight'       => .58,
                    ],
                ],
            ],
            [
                'lib'  => 'Guarantee period',
                'unit' => 'years',
                'product_types' => [
                    [
                        'type'         => 'Radiators',
                        'isBeneficial' => true,
                        'weight'       => .1,
                    ],
                ],
            ],
            [
                'lib'  => 'Power',
                'unit' => 'kW',
                'product_types' => [
                    [
                        'type'         => 'Radiators',
                        'isBeneficial' => true,
                        'weight'       => .0721,
                    ],
                ],
            ],
            [
                'lib'  => 'Appearance',
                'unit' => null,
                'product_types' => [
                    [
                        'type'         => 'Radiators',
                        'isBeneficial' => true,
                        'weight'       => .182,
                    ],
                ],
            ],
            [
                'lib'  => 'Size',
                'unit' => 'm2',
                'product_types' => [
                    [
                        'type'         => 'Radiators',
                        'isBeneficial' => false,
                        'weight'       => .0563,
                    ],
                ],
            ],
            [
                'lib'  => 'Longevity',
                'unit' => 'years',
                'product_types' => [
                    [
                        'type'         => 'Radiators',
                        'isBeneficial' => true,
                        'weight'       => .038,
                    ],
                    [
                        'type'         => 'Thermostatic valves',
                        'isBeneficial' => true,
                        'weight'       => .05,
                    ],
                    [
                        'type'         => 'Pipes',
                        'isBeneficial' => true,
                        'weight'       => .5,
                    ],
                ],
            ],
            [
                'lib'  => 'Weight',
                'unit' => 'kg',
                'product_types' => [
                    [
                        'type'         => 'Radiators',
                        'isBeneficial' => false,
                        'weight'       => .0312,
                    ],
                    [
                        'type'         => 'Thermostatic valves',
                        'isBeneficial' => false,
                        'weight'       => .05,
                    ],
                ],
            ],
            [
                'lib'  => 'Pay-back period',
                'unit' => 'days',
                'product_types' => [
                    [
                        'type'         => 'Radiators',
                        'isBeneficial' => true,
                        'weight'       => .0416,
                    ],
                ],
            ],
            [
                'lib'  => 'Max temperature',
                'unit' => 'celsius',
                'product_types' => [
                    [
                        'type'         => 'Radiators',
                        'isBeneficial' => true,
                        'weight'       => .0488,
                    ],
                    [
                        'type'         => 'Thermostatic valves',
                        'isBeneficial' => true,
                        'weight'       => .1,
                    ],
                    [
                        'type'         => 'Balancing valves',
                        'isBeneficial' => true,
                        'weight'       => .01,
                    ],
                    [
                        'type'         => 'Circulation pump',
                        'isBeneficial' => true,
                        'weight'       => .053,
                    ],
                ],
            ],
            [
                'lib'  => 'Min temperature',
                'unit' => 'celsius',
                'product_types' => [
                    [
                        'type'         => 'Radiators',
                        'isBeneficial' => true,
                        'weight'       => .03,
                    ],
                    [
                        'type'         => 'Thermostatic valves',
                        'isBeneficial' => false,
                        'weight'       => .1,
                    ],
                    [
                        'type'         => 'Balancing valves',
                        'isBeneficial' => false,
                        'weight'       => .01,
                    ],
                    [
                        'type'         => 'Circulation pump',
                        'isBeneficial' => false,
                        'weight'       => .061,
                    ],
                ],
            ],
            [
                'lib'  => 'Diameter',
                'unit' => 'mm',
                'product_types' => [
                    [
                        'type'         => 'Pipes',
                        'isBeneficial' => true,
                        'weight'       => .05,
                    ],
                    [
                        'type'         => 'Circulation pump',
                        'isBeneficial' => true,
                        'weight'       => .031,
                    ],
                ],
            ],
            [
                'lib'  => 'Depth',
                'unit' => 'mm',
                'product_types' => [
                    [
                        'type'         => 'Thermostatic valves',
                        'isBeneficial' => false,
                        'weight'       => .05,
                    ],
                ],
            ],
            [
                'lib'  => 'Height',
                'unit' => 'mm',
                'product_types' => [
                    [
                        'type'         => 'Thermostatic valves',
                        'isBeneficial' => false,
                        'weight'       => .025,
                    ],
                ],
            ],
            [
                'lib'  => 'Precision',
                'unit' => 'celsius',
                'product_types' => [
                    [
                        'type'         => 'Thermostatic valves',
                        'isBeneficial' => false,
                        'weight'       => .05,
                    ],
                ],
            ],
            [
                'lib'  => 'Max pressure',
                'unit' => 'Bar',
                'product_types' => [
                    [
                        'type'         => 'Balancing valves',
                        'isBeneficial' => true,
                        'weight'       => .02,
                    ],
                    [
                        'type'         => 'Circulation pump',
                        'isBeneficial' => true,
                        'weight'       => .058,
                    ],
                ],
            ],
            [
                'lib'  => 'Nominal pressure',
                'unit' => 'Bar',
                'product_types' => [
                    [
                        'type'         => 'Balancing valves',
                        'isBeneficial' => true,
                        'weight'       => .1,
                    ],
                ],
            ],
            [
                'lib'  => 'Flow (KvS)',
                'unit' => 'm3/h',
                'product_types' => [
                    [
                        'type'         => 'Balancing valves',
                        'isBeneficial' => true,
                        'weight'       => .1,
                    ],
                    [
                        'type'         => 'Circulation pump',
                        'isBeneficial' => true,
                        'weight'       => .072,
                    ],
                ],
            ],
            [
                'lib'  => 'Voltage',
                'unit' => 'V',
                'product_types' => [
                    [
                        'type'         => 'Circulation pump',
                        'isBeneficial' => false,
                        'weight'       => .017,
                    ],
                ],
            ],
            [
                'lib'  => 'Frequency',
                'unit' => 'Hz',
                'product_types' => [
                    [
                        'type'         => 'Circulation pump',
                        'isBeneficial' => true,
                        'weight'       => .052,
                    ],
                ],
            ],
            [
                'lib'  => 'Length',
                'unit' => 'cm',
                'product_types' => [
                    [
                        'type'         => 'Radiators',
                        'isBeneficial' => true,
                        'weight'       => .0416,
                    ],
                    [
                        'type'         => 'Thermostatic valves',
                        'isBeneficial' => false,
                        'weight'       => .025,
                    ],
                    [
                        'type'         => 'Pipes',
                        'isBeneficial' => true,
                        'weight'       => .05,
                    ],
                ],
            ],
            [
                'lib'  => 'Delay',
                'unit' => 'minutes',
                'product_types' => [
                    [
                        'type'         => 'Thermostatic valves',
                        'isBeneficial' => false,
                        'weight'       => .05,
                    ],
                ],
            ],
            [
                'lib'  => 'Material',
                'unit' => null,
                'product_types' => [
                    [
                        'type'         => 'Pipes',
                        'isBeneficial' => false,
                        'weight'       => .05,
                    ],
                    [
                        'type'         => 'Balancing valves',
                        'isBeneficial' => true,
                        'weight'       => .025,
                    ],
                ],
            ],
            [
                'lib'  => 'Cover and Disk',
                'unit' => null,
                'product_types' => [
                    [
                        'type'         => 'Balancing valves',
                        'isBeneficial' => true,
                        'weight'       => .025,
                    ],
                ],
            ],
            [
                'lib'  => 'Stem',
                'unit' => null,
                'product_types' => [
                    [
                        'type'         => 'Balancing valves',
                        'isBeneficial' => true,
                        'weight'       => .01,
                    ],
                ],
            ],
            [
                'lib'  => 'Automating',
                'unit' => null,
                'product_types' => [
                    [
                        'type'         => 'Balancing valves',
                        'isBeneficial' => true,
                        'weight'       => .2,
                    ],
                ],
            ],
            [
                'lib'  => 'Easy to use',
                'unit' => null,
                'product_types' => [
                    [
                        'type'         => 'Circulation pump',
                        'isBeneficial' => true,
                        'weight'       => .043,
                    ],
                ],
            ],
            [
                'lib'  => 'Easy to install',
                'unit' => null,
                'product_types' => [
                    [
                        'type'         => 'Circulation pump',
                        'isBeneficial' => true,
                        'weight'       => .033,
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [
            ProductTypeFixtures::class,
        ];
    }
}
