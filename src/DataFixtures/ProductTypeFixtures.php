<?php

namespace App\DataFixtures;

use App\Entity\Criteria;
use App\Entity\ProductType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ProductTypeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $thermostaticValves = new ProductType;
        $thermostaticValves->setLib('Thermostatic valves');

        $radiators = new ProductType;
        $radiators->setLib('Radiators');

        $pipes = new ProductType;
        $pipes->setLib('Pipes');

        $balancingValves = new ProductType;
        $balancingValves->setLib('Balancing valves');

        $circulationPump = new ProductType;
        $circulationPump->setLib('Circulation pump');

        $manager->persist($thermostaticValves);
        $manager->persist($radiators);
        $manager->persist($pipes);
        $manager->persist($balancingValves);
        $manager->persist($circulationPump);
        $manager->flush();
    }
}
