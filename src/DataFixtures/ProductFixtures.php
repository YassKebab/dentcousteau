<?php

namespace App\DataFixtures;

use App\Entity\Criteria;
use App\Entity\Product;
use App\Entity\ProductCriteria;
use App\Entity\ProductType;
use App\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var Project $project */
        $project = $manager->getRepository(Project::class)->findAll()[0];

        $productTypes = $manager
            ->getRepository(ProductType::class)
            ->findAll();

        $criterias = $manager
            ->getRepository(Criteria::class)
            ->findAll();

        foreach ($this->getProductArray() as $productRow) {
            $product = new Product;
            $product->setName($productRow['name']);

            foreach ($productTypes as $productType) {
                if ($productRow['type'] !== $productType->getLib()) {
                    continue;
                }
                $product->setProductType($productType);
                break;
            }

            foreach ($productRow['criterias'] as $criteriaRow) {
                $productCriteria = new ProductCriteria;
                $productCriteria->setProduct($product);
                $productCriteria->setValue((string)$criteriaRow['value']);
                $productCriteria->setNumericValue($criteriaRow['numericValue']);
                foreach ($criterias as $criteria) {
                    if ($criteriaRow['lib'] !== $criteria->getLib()) {
                        continue;
                    }

                    $productCriteria->setCriteria($criteria);
                    break;
                }
                $manager->persist($productCriteria);
            }

            if (in_array($product->getName(), ['Uponor', 'Drayton', 'Wednesbury Copper Pipe 15mm x 3m', 'Ayvaz BSBV-200 Static 100 DN', 'Grundfos Alpha 1L'])) {
                $product->addProject($project);
                $manager->persist($project);
            }

            $manager->persist($product);
        }
        $manager->flush();
    }

    protected function getProductArray(): array
    {
        return [
            [
                'name'      => 'Uponor',
                'type'      => 'Radiators',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => 63,
                        'numericValue' => 63
                    ],
                    [
                        'lib'          => 'Guarantee period',
                        'value'        => 10,
                        'numericValue' => 10
                    ],
                    [
                        'lib'          => 'Power',
                        'value'        => 900,
                        'numericValue' => 900
                    ],
                    [
                        'lib'          => 'Appearance',
                        'value'        => '0.5',
                        'numericValue' => .5
                    ],
                    [
                        'lib'          => 'Size',
                        'value'        => '0.32',
                        'numericValue' => .32
                    ],
                    [
                        'lib'          => 'Longevity',
                        'value'        => 20,
                        'numericValue' => 20
                    ],
                    [
                        'lib'          => 'Weight',
                        'value'        => 11,
                        'numericValue' => 11
                    ],
                    [
                        'lib'          => 'Pay-back period',
                        'value'        => 14,
                        'numericValue' => 14
                    ],
                    [
                        'lib'          => 'Max temperature',
                        'value'        => 70,
                        'numericValue' => 70
                    ],
                    [
                        'lib'          => 'Min temperature',
                        'value'        => 55,
                        'numericValue' => 55
                    ],
                ],
            ],
            [
                'name'      => 'Kermi',
                'type'      => 'Radiators',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => 75,
                        'numericValue' => 75
                    ],
                    [
                        'lib'          => 'Guarantee period',
                        'value'        => 10,
                        'numericValue' => 10
                    ],
                    [
                        'lib'          => 'Power',
                        'value'        => 992,
                        'numericValue' => 992
                    ],
                    [
                        'lib'          => 'Appearance',
                        'value'        => '0.6',
                        'numericValue' => .6
                    ],
                    [
                        'lib'          => 'Size',
                        'value'        => '0.4',
                        'numericValue' => .4
                    ],
                    [
                        'lib'          => 'Longevity',
                        'value'        => 16,
                        'numericValue' => 16
                    ],
                    [
                        'lib'          => 'Weight',
                        'value'        => '12.3',
                        'numericValue' => 12.3
                    ],
                    [
                        'lib'          => 'Pay-back period',
                        'value'        => 14,
                        'numericValue' => 14
                    ],
                    [
                        'lib'          => 'Max temperature',
                        'value'        => 70,
                        'numericValue' => 70
                    ],
                    [
                        'lib'          => 'Min temperature',
                        'value'        => 55,
                        'numericValue' => 55
                    ],
                ],
            ],
            [
                'name'      => 'Termolux',
                'type'      => 'Radiators',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => 84,
                        'numericValue' => 84
                    ],
                    [
                        'lib'          => 'Guarantee period',
                        'value'        => 8,
                        'numericValue' => 8
                    ],
                    [
                        'lib'          => 'Power',
                        'value'        => 974,
                        'numericValue' => 974
                    ],
                    [
                        'lib'          => 'Appearance',
                        'value'        => '0.6',
                        'numericValue' => .6
                    ],
                    [
                        'lib'          => 'Size',
                        'value'        => '0.38',
                        'numericValue' => .38
                    ],
                    [
                        'lib'          => 'Longevity',
                        'value'        => 18,
                        'numericValue' => 18
                    ],
                    [
                        'lib'          => 'Weight',
                        'value'        => '12.1',
                        'numericValue' => 12.1
                    ],
                    [
                        'lib'          => 'Pay-back period',
                        'value'        => 14,
                        'numericValue' => 14
                    ],
                    [
                        'lib'          => 'Max temperature',
                        'value'        => 70,
                        'numericValue' => 70
                    ],
                    [
                        'lib'          => 'Min temperature',
                        'value'        => 55,
                        'numericValue' => 55
                    ],
                ],
            ],
            [
                'name'      => 'Korado',
                'type'      => 'Radiators',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => 55,
                        'numericValue' => 55
                    ],
                    [
                        'lib'          => 'Guarantee period',
                        'value'        => 6,
                        'numericValue' => 6
                    ],
                    [
                        'lib'          => 'Power',
                        'value'        => 923,
                        'numericValue' => 923
                    ],
                    [
                        'lib'          => 'Appearance',
                        'value'        => '0.5',
                        'numericValue' => .5
                    ],
                    [
                        'lib'          => 'Size',
                        'value'        => '0.33',
                        'numericValue' => .33
                    ],
                    [
                        'lib'          => 'Longevity',
                        'value'        => 15,
                        'numericValue' => 15
                    ],
                    [
                        'lib'          => 'Weight',
                        'value'        => '11.9',
                        'numericValue' => 11.9
                    ],
                    [
                        'lib'          => 'Pay-back period',
                        'value'        => 14,
                        'numericValue' => 14
                    ],
                    [
                        'lib'          => 'Max temperature',
                        'value'        => 70,
                        'numericValue' => 70
                    ],
                    [
                        'lib'          => 'Min temperature',
                        'value'        => 55,
                        'numericValue' => 55
                    ],
                ],
            ],
            [
                'name'      => 'Drayton',
                'type'      => 'Thermostatic valves',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => '21.06',
                        'numericValue' => 21.06
                    ],
                    [
                        'lib'          => 'Weight',
                        'value'        => 322,
                        'numericValue' => 322
                    ],
                    [
                        'lib'          => 'Longevity',
                        'value'        => 1,
                        'numericValue' => 1
                    ],
                    [
                        'lib'          => 'Max temperature',
                        'value'        => 30,
                        'numericValue' => 30
                    ],
                    [
                        'lib'          => 'Min temperature',
                        'value'        => 8,
                        'numericValue' => 8
                    ],
                    [
                        'lib'          => 'Precision',
                        'value'        => '0.5',
                        'numericValue' => .5
                    ],
                    [
                        'lib'          => 'Delay',
                        'value'        => 20,
                        'numericValue' => 20
                    ],
                    [
                        'lib'          => 'Depth',
                        'value'        => 83,
                        'numericValue' => 83
                    ],
                    [
                        'lib'          => 'Height',
                        'value'        => 53,
                        'numericValue' => 53
                    ],
                    [
                        'lib'          => 'Length',
                        'value'        => 82,
                        'numericValue' => 82
                    ],
                ],
            ],
            [
                'name'      => 'Terrier',
                'type'      => 'Thermostatic valves',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => '31.31',
                        'numericValue' => 31.31
                    ],
                    [
                        'lib'          => 'Weight',
                        'value'        => 421,
                        'numericValue' => 421
                    ],
                    [
                        'lib'          => 'Longevity',
                        'value'        => 5,
                        'numericValue' => 5
                    ],
                    [
                        'lib'          => 'Max temperature',
                        'value'        => 28,
                        'numericValue' => 28
                    ],
                    [
                        'lib'          => 'Min temperature',
                        'value'        => 0,
                        'numericValue' => 0
                    ],
                    [
                        'lib'          => 'Precision',
                        'value'        => 1,
                        'numericValue' => 1
                    ],
                    [
                        'lib'          => 'Delay',
                        'value'        => 23,
                        'numericValue' => 23
                    ],
                    [
                        'lib'          => 'Depth',
                        'value'        => 71,
                        'numericValue' => 71
                    ],
                    [
                        'lib'          => 'Height',
                        'value'        => 83,
                        'numericValue' => 83
                    ],
                    [
                        'lib'          => 'Length',
                        'value'        => 87,
                        'numericValue' => 87
                    ],
                ],
            ],
            [
                'name'      => 'Danfoss',
                'type'      => 'Thermostatic valves',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => '10.67',
                        'numericValue' => 10.67
                    ],
                    [
                        'lib'          => 'Weight',
                        'value'        => 286,
                        'numericValue' => 286
                    ],
                    [
                        'lib'          => 'Longevity',
                        'value'        => 1,
                        'numericValue' => 1
                    ],
                    [
                        'lib'          => 'Max temperature',
                        'value'        => 30,
                        'numericValue' => 30
                    ],
                    [
                        'lib'          => 'Min temperature',
                        'value'        => 5,
                        'numericValue' => 5
                    ],
                    [
                        'lib'          => 'Precision',
                        'value'        => 1,
                        'numericValue' => 1
                    ],
                    [
                        'lib'          => 'Delay',
                        'value'        => 30,
                        'numericValue' => 30
                    ],
                    [
                        'lib'          => 'Depth',
                        'value'        => 72,
                        'numericValue' => 72
                    ],
                    [
                        'lib'          => 'Height',
                        'value'        => 65,
                        'numericValue' => 65
                    ],
                    [
                        'lib'          => 'Length',
                        'value'        => 73,
                        'numericValue' => 73
                    ],
                ],
            ],
            [
                'name'      => 'Pegler',
                'type'      => 'Thermostatic valves',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => '33.76',
                        'numericValue' => 33.76
                    ],
                    [
                        'lib'          => 'Weight',
                        'value'        => 390,
                        'numericValue' => 390
                    ],
                    [
                        'lib'          => 'Longevity',
                        'value'        => 3,
                        'numericValue' => 3
                    ],
                    [
                        'lib'          => 'Max temperature',
                        'value'        => 27,
                        'numericValue' => 27
                    ],
                    [
                        'lib'          => 'Min temperature',
                        'value'        => 7,
                        'numericValue' => 7
                    ],
                    [
                        'lib'          => 'Precision',
                        'value'        => '0.5',
                        'numericValue' => .5
                    ],
                    [
                        'lib'          => 'Delay',
                        'value'        => 15,
                        'numericValue' => 15
                    ],
                    [
                        'lib'          => 'Depth',
                        'value'        => 76,
                        'numericValue' => 76
                    ],
                    [
                        'lib'          => 'Height',
                        'value'        => 72,
                        'numericValue' => 72
                    ],
                    [
                        'lib'          => 'Length',
                        'value'        => 71,
                        'numericValue' => 71
                    ],
                ],
            ],
            [
                'name'      => 'Ayvaz BSBV-200 Static 100 DN',
                'type'      => 'Balancing valves',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => '437.4',
                        'numericValue' => 437.4
                    ],
                    [
                        'lib'          => 'Max pressure',
                        'value'        => 16,
                        'numericValue' => 16
                    ],
                    [
                        'lib'          => 'Nominal pressure',
                        'value'        => 16,
                        'numericValue' => 16
                    ],
                    [
                        'lib'          => 'Max temperature',
                        'value'        => 120,
                        'numericValue' => 120
                    ],
                    [
                        'lib'          => 'Min temperature',
                        'value'        => -10,
                        'numericValue' => -10
                    ],
                    [
                        'lib'          => 'Flow (KvS)',
                        'value'        => '184.7',
                        'numericValue' => 184.7
                    ],
                    [
                        'lib'          => 'Cover and Disk',
                        'value'        => 'GG-25 Cast Iron Composite',
                        'numericValue' => .0025
                    ],
                    [
                        'lib'          => 'Material',
                        'value'        => 'Ductile Iron',
                        'numericValue' => .0025
                    ],
                    [
                        'lib'          => 'Stem',
                        'value'        => 'Brass',
                        'numericValue' => .0025
                    ],
                    [
                        'lib'          => 'Automating',
                        'value'        => 'Static',
                        'numericValue' => 0
                    ],
                ],
            ],
            [
                'name'      => 'Ayvaz BSBV-100 Static 50 DN',
                'type'      => 'Balancing valves',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => '40.7',
                        'numericValue' => 40.7
                    ],
                    [
                        'lib'          => 'Max pressure',
                        'value'        => 25,
                        'numericValue' => 25
                    ],
                    [
                        'lib'          => 'Nominal pressure',
                        'value'        => 16,
                        'numericValue' => 16
                    ],
                    [
                        'lib'          => 'Max temperature',
                        'value'        => 120,
                        'numericValue' => 120
                    ],
                    [
                        'lib'          => 'Min temperature',
                        'value'        => -10,
                        'numericValue' => -10
                    ],
                    [
                        'lib'          => 'Flow (KvS)',
                        'value'        => '43.2',
                        'numericValue' => 43.2
                    ],
                    [
                        'lib'          => 'Cover and Disk',
                        'value'        => 'Brass',
                        'numericValue' => .005
                    ],
                    [
                        'lib'          => 'Material',
                        'value'        => 'Bronze',
                        'numericValue' => .005
                    ],
                    [
                        'lib'          => 'Stem',
                        'value'        => 'Brass',
                        'numericValue' => .0025
                    ],
                    [
                        'lib'          => 'Automating',
                        'value'        => 'Static',
                        'numericValue' => 0
                    ],
                ],
            ],
            [
                'name'      => 'Ayvaz DBV-40 Dynamic 100 DN',
                'type'      => 'Balancing valves',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => '938.4',
                        'numericValue' => 938.4
                    ],
                    [
                        'lib'          => 'Max pressure',
                        'value'        => 6,
                        'numericValue' => 6
                    ],
                    [
                        'lib'          => 'Nominal pressure',
                        'value'        => 6,
                        'numericValue' => 6
                    ],
                    [
                        'lib'          => 'Max temperature',
                        'value'        => 120,
                        'numericValue' => 120
                    ],
                    [
                        'lib'          => 'Min temperature',
                        'value'        => -20,
                        'numericValue' => -20
                    ],
                    [
                        'lib'          => 'Flow (KvS)',
                        'value'        => '66',
                        'numericValue' => 66
                    ],
                    [
                        'lib'          => 'Cover and Disk',
                        'value'        => 'Brass Stainless Steel',
                        'numericValue' => .01
                    ],
                    [
                        'lib'          => 'Material',
                        'value'        => 'St 37 Carbon Steel',
                        'numericValue' => .01
                    ],
                    [
                        'lib'          => 'Stem',
                        'value'        => 'Brass',
                        'numericValue' => .0025
                    ],
                    [
                        'lib'          => 'Automating',
                        'value'        => 'Dynamic',
                        'numericValue' => .1
                    ],
                ],
            ],
            [
                'name'      => "Ayvaz DBV-30 Dynamic 2''",
                'type'      => 'Balancing valves',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => '122.4',
                        'numericValue' => 122.4
                    ],
                    [
                        'lib'          => 'Max pressure',
                        'value'        => 3,
                        'numericValue' => 3
                    ],
                    [
                        'lib'          => 'Nominal pressure',
                        'value'        => 3,
                        'numericValue' => 3
                    ],
                    [
                        'lib'          => 'Max temperature',
                        'value'        => 120,
                        'numericValue' => 120
                    ],
                    [
                        'lib'          => 'Min temperature',
                        'value'        => -20,
                        'numericValue' => -20
                    ],
                    [
                        'lib'          => 'Flow (KvS)',
                        'value'        => '80',
                        'numericValue' => 80
                    ],
                    [
                        'lib'          => 'Cover and Disk',
                        'value'        => 'Brass Stainless Steel',
                        'numericValue' => .0075
                    ],
                    [
                        'lib'          => 'Material',
                        'value'        => 'Brass',
                        'numericValue' => .0075
                    ],
                    [
                        'lib'          => 'Stem',
                        'value'        => 'Brass',
                        'numericValue' => .0025
                    ],
                    [
                        'lib'          => 'Automating',
                        'value'        => 'Dynamic',
                        'numericValue' => .1
                    ],
                ],
            ],
            [
                'name'      => 'Wednesbury Copper Pipe 15mm x 3m',
                'type'      => 'Pipes',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => '7.95',
                        'numericValue' => 7.95
                    ],
                    [
                        'lib'          => 'Diameter',
                        'value'        => 15,
                        'numericValue' => 15
                    ],
                    [
                        'lib'          => 'Material',
                        'value'        => 'Copper',
                        'numericValue' => 1
                    ],
                    [
                        'lib'          => 'Longevity',
                        'value'        => 25,
                        'numericValue' => 25
                    ],
                    [
                        'lib'          => 'Length',
                        'value'        => 300,
                        'numericValue' => 300
                    ],
                ],
            ],
            [
                'name'      => 'Wednesbury Copper Pipe 22mm x 3m',
                'type'      => 'Pipes',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => '15.45',
                        'numericValue' => 15.45
                    ],
                    [
                        'lib'          => 'Diameter',
                        'value'        => 22,
                        'numericValue' => 22
                    ],
                    [
                        'lib'          => 'Material',
                        'value'        => 'Copper',
                        'numericValue' => 1
                    ],
                    [
                        'lib'          => 'Longevity',
                        'value'        => 25,
                        'numericValue' => 25
                    ],
                    [
                        'lib'          => 'Length',
                        'value'        => 300,
                        'numericValue' => 300
                    ],
                ],
            ],
            [
                'name'      => 'Wednesbury Chrome Pipe 15mm x 2m',
                'type'      => 'Pipes',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => '14.45',
                        'numericValue' => 14.45
                    ],
                    [
                        'lib'          => 'Diameter',
                        'value'        => 15,
                        'numericValue' => 15
                    ],
                    [
                        'lib'          => 'Material',
                        'value'        => 'Chrome',
                        'numericValue' => 1
                    ],
                    [
                        'lib'          => 'Longevity',
                        'value'        => 25,
                        'numericValue' => 25
                    ],
                    [
                        'lib'          => 'Length',
                        'value'        => 200,
                        'numericValue' => 200
                    ],
                ],
            ],
            [
                'name'      => 'FloFit Polybutylene Barrier Pipe Grey 100m x 15mm',
                'type'      => 'Pipes',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => '103.95',
                        'numericValue' => 103.95
                    ],
                    [
                        'lib'          => 'Diameter',
                        'value'        => 15,
                        'numericValue' => 15
                    ],
                    [
                        'lib'          => 'Material',
                        'value'        => 'Polybutylene',
                        'numericValue' => 1
                    ],
                    [
                        'lib'          => 'Longevity',
                        'value'        => 25,
                        'numericValue' => 25
                    ],
                    [
                        'lib'          => 'Length',
                        'value'        => 100,
                        'numericValue' => 100
                    ],
                ],
            ],
            [
                'name'      => 'Omni 25-60 / 180',
                'type'      => 'Circulation pump',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => '59.9',
                        'numericValue' => 59.9
                    ],
                    [
                        'lib'          => 'Voltage',
                        'value'        => 230,
                        'numericValue' => 230
                    ],
                    [
                        'lib'          => 'Frequency',
                        'value'        => 50,
                        'numericValue' => 50
                    ],
                    [
                        'lib'          => 'Max pressure',
                        'value'        => 10,
                        'numericValue' => 10
                    ],
                    [
                        'lib'          => 'Easy to use',
                        'value'        => '0.67',
                        'numericValue' => .67,
                    ],
                    [
                        'lib'          => 'Easy to install',
                        'value'        => '0.74',
                        'numericValue' => .74,
                    ],
                    [
                        'lib'          => 'Flow (KvS)',
                        'value'        => 3,
                        'numericValue' => 3,
                    ],
                    [
                        'lib'          => 'Min temperature',
                        'value'        => 5,
                        'numericValue' => 5,
                    ],
                    [
                        'lib'          => 'Max temperature',
                        'value'        => 120,
                        'numericValue' => 120,
                    ],
                    [
                        'lib'          => 'Diameter',
                        'value'        => '38.1',
                        'numericValue' => 38.1,
                    ],
                ],
            ],
            [
                'name'      => 'EVOSTA 2 40-70/180',
                'type'      => 'Circulation pump',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => '107.32',
                        'numericValue' => 107.32
                    ],
                    [
                        'lib'          => 'Voltage',
                        'value'        => 230,
                        'numericValue' => 230
                    ],
                    [
                        'lib'          => 'Frequency',
                        'value'        => 60,
                        'numericValue' => 60
                    ],
                    [
                        'lib'          => 'Max pressure',
                        'value'        => 10,
                        'numericValue' => 10
                    ],
                    [
                        'lib'          => 'Easy to use',
                        'value'        => '0.79',
                        'numericValue' => .79,
                    ],
                    [
                        'lib'          => 'Easy to install',
                        'value'        => '0.61',
                        'numericValue' => .61,
                    ],
                    [
                        'lib'          => 'Flow (KvS)',
                        'value'        => '3.6',
                        'numericValue' => 3.6,
                    ],
                    [
                        'lib'          => 'Min temperature',
                        'value'        => -10,
                        'numericValue' => -10,
                    ],
                    [
                        'lib'          => 'Max temperature',
                        'value'        => 110,
                        'numericValue' => 110,
                    ],
                    [
                        'lib'          => 'Diameter',
                        'value'        => '38.1',
                        'numericValue' => 38.1,
                    ],
                ],
            ],
            [
                'name'      => 'SALMSON Priux Home',
                'type'      => 'Circulation pump',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => '253.68',
                        'numericValue' => 253.68
                    ],
                    [
                        'lib'          => 'Voltage',
                        'value'        => 230,
                        'numericValue' => 230
                    ],
                    [
                        'lib'          => 'Frequency',
                        'value'        => 50,
                        'numericValue' => 50
                    ],
                    [
                        'lib'          => 'Max pressure',
                        'value'        => 6,
                        'numericValue' => 6
                    ],
                    [
                        'lib'          => 'Easy to use',
                        'value'        => '0.63',
                        'numericValue' => .63,
                    ],
                    [
                        'lib'          => 'Easy to install',
                        'value'        => '0.59',
                        'numericValue' => .59,
                    ],
                    [
                        'lib'          => 'Flow (KvS)',
                        'value'        => 5,
                        'numericValue' => 5,
                    ],
                    [
                        'lib'          => 'Min temperature',
                        'value'        => -10,
                        'numericValue' => -10,
                    ],
                    [
                        'lib'          => 'Max temperature',
                        'value'        => 95,
                        'numericValue' => 95,
                    ],
                    [
                        'lib'          => 'Diameter',
                        'value'        => '31.75',
                        'numericValue' => 31.75,
                    ],
                ],
            ],
            [
                'name'      => 'Grundfos Alpha 1L',
                'type'      => 'Circulation pump',
                'criterias' => [
                    [
                        'lib'          => 'Price',
                        'value'        => '129.5',
                        'numericValue' => 129.5
                    ],
                    [
                        'lib'          => 'Voltage',
                        'value'        => 230,
                        'numericValue' => 230
                    ],
                    [
                        'lib'          => 'Frequency',
                        'value'        => 50,
                        'numericValue' => 50
                    ],
                    [
                        'lib'          => 'Max pressure',
                        'value'        => 10,
                        'numericValue' => 10
                    ],
                    [
                        'lib'          => 'Easy to use',
                        'value'        => '0.73',
                        'numericValue' => .73,
                    ],
                    [
                        'lib'          => 'Easy to install',
                        'value'        => '0.83',
                        'numericValue' => .83,
                    ],
                    [
                        'lib'          => 'Flow (KvS)',
                        'value'        => '3.6',
                        'numericValue' => 3.6,
                    ],
                    [
                        'lib'          => 'Min temperature',
                        'value'        => 2,
                        'numericValue' => 2,
                    ],
                    [
                        'lib'          => 'Max temperature',
                        'value'        => 95,
                        'numericValue' => 95,
                    ],
                    [
                        'lib'          => 'Diameter',
                        'value'        => '38.1',
                        'numericValue' => 38.1,
                    ],
                ],
            ],
        ];
    }

    public function getDependencies()
    {
        return [
            CriteriaFixtures::class,
            ProjectFixtures::class
        ];
    }
}
