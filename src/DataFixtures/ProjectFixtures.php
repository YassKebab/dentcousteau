<?php

namespace App\DataFixtures;

use App\Entity\ProductType;
use App\Entity\ProductTypeProject;
use App\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ProjectFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $project = new Project;
        $project->setName('Scheme 4');
        $manager->persist($project);

        $productTypes = $manager
            ->getRepository(ProductType::class)
            ->findAll();

        foreach ($productTypes as $productType) {
            $productProjectType = new ProductTypeProject;
            $productProjectType->setProductType($productType);
            $productProjectType->setProject($project);
            $productProjectType->setNb(1);
            $manager->persist($productProjectType);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ProductTypeFixtures::class,
        ];
    }
}
