<?php


namespace App\Service;


use App\Entity\Criteria;
use App\Entity\CriteriaProductType;
use App\Entity\ProductType;
use App\Entity\Project;

class CoprasProcessor
{
    /**
     * @param Project $project
     * @return array
     */
    public function process(Project $project): array
    {
        $response = ['message' => ''];
        $res = [];

        if ($project->getProjectProductTypes()->count() === 0) {
            $response['message'] = sprintf('No product type for the project %s.', $project->getName());
            return $response;
        }

        foreach ($project->getProjectProductTypes() as $projectProductType) {
            /** @var ProductType $productType */
            $productType = $projectProductType->getProductType();
            $res[$productType->getLib()] = [];

            if ($productType->getProductTypeCriterias()->count() === 0) {
                $response['message'] = sprintf('No criteria for the product type %s.', $productType->getLib());
                return $response;
            }

            $weightSum = 0;
            foreach ($productType->getProductTypeCriterias() as $productTypeCriteria) {
                $weightSum += $productTypeCriteria->getWeight();
            }

            if (abs(1 - $weightSum) > .1) {
                $response['message'] = sprintf('Bad setup for product type %s weights : %d.', $productType->getLib(), $weightSum);
                return $response;
            }

            $normalizedValues = [];
            $minusWeightSum = 0;

            foreach ($productType->getProducts() as $product) {
                $criteriaSum = 0;
                foreach ($product->getProductCriterias() as $productCriteria) {
                    /** @var Criteria $criteria */
                    $criteria = $productCriteria->getCriteria();

                    $productTypeCriteria = new CriteriaProductType;

                    foreach ($criteria->getCriteriaProductTypes() as $criteriaProductType) {
                        if ($criteriaProductType->getProductType()->getId() !== $productType->getId()) {
                            continue;
                        }
                        $productTypeCriteria = $criteriaProductType;
                        break;
                    }

                    if (is_null($productTypeCriteria->getId())) {
                        $response['message'] = sprintf('No product type for criteria %s.', $criteria->getLib());
                        return $response;
                    }

                    /** @var float $weight */
                    $weight = $productTypeCriteria->getWeight();
                    /** @var bool $isBeneficial */
                    $isBeneficial = $productTypeCriteria->getIsBeneficial();

                    if (!$isBeneficial) {
                        $minusWeightSum += $weight;
                    }

                    $criteriaSum += $productCriteria->getNumericValue();
                }

                foreach ($product->getProductCriterias() as $productCriteria) {
                    /** @var Criteria $criteria */
                    $criteria = $productCriteria->getCriteria();
                    $productTypeCriteria = new CriteriaProductType;

                    foreach ($criteria->getCriteriaProductTypes() as $criteriaProductType) {
                        if ($criteriaProductType->getProductType()->getId() !== $productType->getId()) {
                            continue;
                        }
                        $productTypeCriteria = $criteriaProductType;
                        break;
                    }

                    /** @var float $weight */
                    $weight = $productTypeCriteria->getWeight();
                    /** @var bool $isBeneficial */
                    $isBeneficial = $productTypeCriteria->getIsBeneficial();

                    $normalizedValue = $productCriteria->getNumericValue() * $weight / $criteriaSum;

                    if (!array_key_exists($product->getId(), $normalizedValues)) {
                        $normalizedValues[$product->getId()] = [];
                    }

                    if (!array_key_exists((int)$isBeneficial, $normalizedValues[$product->getId()])) {
                        $normalizedValues[$product->getId()][(int)$isBeneficial] = [];
                    }

                    if (!array_key_exists($criteria->getId(), $normalizedValues[$product->getId()][(int)$isBeneficial])) {
                        $normalizedValues[$product->getId()][(int)$isBeneficial][$criteria->getId()] = [];
                    }

                    $normalizedValues[$product->getId()][(int)$isBeneficial][$criteria->getId()] = [
                        'normalized_value' => $normalizedValue,
                        'product_name'     => $product->getName()
                    ];
                }
            }

            $productData = [];
            $oneHoverMinusesSum = 0;

            foreach ($normalizedValues as $productId => $productValues) {
                $sumPluses = 0;
                $sumMinuses = 0;
                $name = '';
                foreach ($productValues as $isBeneficial => $criteriaValue) {
                    if ($isBeneficial) {
                        foreach ($criteriaValue as $criteriaId => $value) {
                            $sumPluses += $value['normalized_value'];
                            $name = $value['product_name'];
                        }
                    } else {
                        foreach ($criteriaValue as $criteriaId => $value) {
                            $sumMinuses += $value['normalized_value'];
                            $name = $value['product_name'];
                        }
                    }
                }
                $productData[$productId] = [
                    'pluses'  => $sumPluses,
                    'minuses' => $sumMinuses,
                    'name'    => $name
                ];
                $oneHoverMinusesSum += 1 / $sumMinuses;
            }

            $q = [];
            foreach ($productData as $productId => $data) {
                $q[$productId] = $minusWeightSum / ($data['minuses'] * $oneHoverMinusesSum) + $data['pluses'];
            }

            $maxProduct = null;
            $maxValue = 0;


            foreach ($q as $productId => $value) {
                if ($value > $maxValue) {
                    $maxValue = $value;
                    $maxProduct = $productId;
                }
            }



            $u = [];
            $u[$maxProduct] = [
                'q'       => $maxValue,
                'percent' => 100
            ];

            foreach ($q as $productId => $value) {
                if ($maxProduct === $productId) {
                    continue;
                }

                $u[$productId] = [
                    'q'       => $value,
                    'percent' => $value / $maxValue * 100,
                ];
            }
            asort($u, SORT_NUMERIC);
            foreach ($u as $productId => $uRes) {
                $res[$productType->getLib()][] = [
                    'name'       => $productData[$productId]['name'],
                    'id'         => $productId,
                    'percentage' => $uRes['percent'],
                ];
            }
        }

        return $res;
    }
}
