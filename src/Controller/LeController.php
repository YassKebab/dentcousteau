<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\ProductCriteria;
use App\Entity\ProductTypeProject;
use App\Entity\Project;
use App\Service\CoprasProcessor;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Form\CriteriaType;
use App\Form\ProductType;
use App\Form\ProjectType;
use App\Form\ProductsCriteriasType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;

class LeController extends AbstractController
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/", name="racine")
     */
    public function RacineRoute()
    {
        $allProjects = $this->getDoctrine()->getManager()
            ->getRepository(Project::class)->findAll();

        return $this->render('projects.html.twig', ["projects" => $allProjects]);
    }

    // ################## PRODUITS #####################

    /**
     * @Route("/produits", name="app_tous_les_produits")
     */
    public function ProduitsRoute(Request $request)
    {
        $form = $this->createForm(ProductType::class);

        $manager = $this->getDoctrine()->getManager()->getRepository(\App\Entity\ProductType::class);
        $component = [
            "components" =>
            [
                "radiators" => $manager->findOneBy(array('lib' => 'Radiators'))->getProducts(),
                "pipes" =>  $manager->findOneBy(array('lib' => 'Pipes'))->getProducts(),
                "thermostaticValves" =>  $manager->findOneBy(array('lib' => 'Thermostatic valves'))->getProducts(),
                "balancingValves" =>  $manager->findOneBy(array('lib' => 'Balancing valves'))->getProducts(),
                "circulatoPumps" =>  $manager->findOneBy(array('lib' => 'Circulation pump'))->getProducts()
            ],
            'form' => $form->createView()
        ];

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $product = new Product();
            $product->setName($form->getData()['name']);

            $productType = $form->getData()['Components_type'];
            $product->setProductType($productType);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();
        }

        return $this->render('produits.html.twig', $component);
    }

    /**
     * @Route("/modifier_produit", name="app_modifier_produit")
     */
    public function ModifierProduit(Request $request)
    {
        /** @var  Product $product */
        $product = $this->getDoctrine()->getManager()
            ->getRepository(Product::class)->findOneBy(array('id' => $request->get('id')));

        if ($product->getProductCriterias()->count() === 0) {
            foreach ($product->getProductType()->getProductTypeCriterias() as $productTypeCriteria) {
                $productCriteria = new ProductCriteria;
                $productCriteria->setCriteria($productTypeCriteria->getCriteria());
                $productCriteria->setProduct($product);
                $product->addProductCriteria($productCriteria);
            }
        }

        $form = $this->createForm(ProductsCriteriasType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            foreach ($product->getProductCriterias() as $criterias) {
                $entityManager->persist($criterias);
            }
            $entityManager->flush();

            return $this->redirectToRoute('app_tous_les_produits');
        }

        return $this->render('edition_produit.html.twig', ['title' => "Component detail", "product" => $product, "form" => $form->createView()]);
    }

    /**
     * @Route("/ajouter_produit", name="app_ajouter_produit")
     */
    public function AjouterProduit()
    {
        $form = $this->createForm(ProductType::class);

        return $this->render('edition_produit.html.twig', ['form' => $form->createView(), 'title' => "New component"]);
    }



    // ################## PROJETS #####################

    /**
     * @Route("/map_products_project", name="app_map_products_project")
     */
    public function MapProductsProject(Request $request)
    {
        $data = $request->request;
        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository(Project::class)->findOneBy(array('id' => $data->get('project_id')));

        $project->addProduct($em->getRepository(Product::class)->findOneBy(array('id' => $data->get('Radiators_choice'))));
        $project->addProduct($em->getRepository(Product::class)->findOneBy(array('id' => $data->get('Pipes_choice'))));
        $project->addProduct($em->getRepository(Product::class)->findOneBy(array('id' => $data->get('Thermostatic_valves_choice'))));
        $project->addProduct($em->getRepository(Product::class)->findOneBy(array('id' => $data->get('Balancing_valves_choice'))));
        $project->addProduct($em->getRepository(Product::class)->findOneBy(array('id' => $data->get('Circulation_pump_choice'))));
        $em->persist($project);

        foreach ($project->getProducts() as $product) {
            $product->addProject($project);
            $em->persist($product);
        }

        $em->flush();

        return $this->redirectToRoute('app_modifier_projet',  array('id' => $project->getId()));
    }

    /**
     * @Route("/modifier_projet", name="app_modifier_projet")
     */
    public function ModifierProjet(Request $request)
    {
        $project = $this->getDoctrine()->getManager()
            ->getRepository(Project::class)->findOneBy(array('id' => $request->get('id')));

        return $this->render('edition_projet.html.twig', [
            'title' => $project->getName(),
            'products' => $project->getProducts()->getValues(),
            'projectId' => $project->getId()
        ]);
    }


    /**
     * @Route("/creer_projet", name="app_creer_projet")
     */
    public function CreerProjetRoute(Request $request)
    {
        $project = new Project;
        $productTypes = $this->getDoctrine()->getManager()->getRepository(\App\Entity\ProductType::class)->findAll();
        foreach ($productTypes as $productType) {
            $productTypeProject = new ProductTypeProject;
            $productTypeProject->setNb(0);
            $productTypeProject->setProductType($productType);
            $productTypeProject->setProject($project);
            $project->addProjectProductTypes($productTypeProject);
        }
        $form = $this->createForm(ProjectType::class, $project);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $project = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($project);
            foreach ($project->getProjectProductTypes() as $projectProductType) {
                $entityManager->persist($projectProductType);
            }
            $entityManager->flush();

            return $this->redirectToRoute('racine');
        }

        return $this->render('add_projet.html.twig', ['form' => $form->createView(), 'title' => "New project from scratch"]);
    }

    /**
     * @Route("/copras", name="copras_action")
     * @param CoprasProcessor $processor
     * @param Request $request
     * @return Response
     */
    public function ajaxAction(CoprasProcessor $processor, Request $request)
    {
        $data = json_decode($request->getContent(), true);
        /** @var Project $project */
        $project = $this->getDoctrine()->getRepository(Project::class)->find($data['project_id']);
        $process = $processor->process($project);
        $response = new Response(json_encode($process));
        if (array_key_exists('message', $process)) {
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function coprasCalcul()
    {
        // TODO : C'est juste un mock
        $entityManager = $this->getDoctrine()->getManager();
        $products =  $entityManager->getRepository(Product::class)->findAll();
        return [
            'radiators' => [
                ['name' => $products[0]->getName(), 'id' => $products[0]->getId(), 'percentage' => 100],
                ['name' => $products[2]->getName(), 'id' => $products[2]->getId(), 'percentage' => 50],
                ['name' => $products[3]->getName(), 'id' => $products[3]->getId(), 'percentage' => 20],
            ],
            'pipes' => [
                ['name' => $products[4]->getName(), 'id' => $products[4]->getId(), 'percentage' => 100],
                ['name' => $products[5]->getName(), 'id' => $products[5]->getId(), 'percentage' => 50],
                ['name' => $products[6]->getName(), 'id' => $products[6]->getId(), 'percentage' => 20],
            ],
            'thermostatic valves' => [
                ['name' => $products[7]->getName(), 'id' => $products[7]->getId(), 'percentage' => 100],
                ['name' => $products[8]->getName(), 'id' => $products[8]->getId(), 'percentage' => 50],
                ['name' => $products[9]->getName(), 'id' => $products[9]->getId(), 'percentage' => 20],
            ],
            'balancing valves' => [
                ['name' => $products[10]->getName(), 'id' => $products[10]->getId(), 'percentage' => 100],
                ['name' => $products[11]->getName(), 'id' => $products[11]->getId(), 'percentage' => 50],
                ['name' => $products[12]->getName(), 'id' => $products[12]->getId(), 'percentage' => 20],
            ],
            'circulator pumps' => [
                ['name' => $products[13]->getName(), 'id' => $products[13]->getId(), 'percentage' => 100],
                ['name' => $products[14]->getName(), 'id' => $products[14]->getId(), 'percentage' => 50],
                ['name' => $products[15]->getName(), 'id' => $products[15]->getId(), 'percentage' => 20],
            ],
        ];
    }
}
