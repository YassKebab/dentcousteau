<?php


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="product")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=ProductCriteria::class, mappedBy="product")
     *
     * @var Collection
     */
    private $productCriterias;

    /**
     * @ORM\ManyToMany(targetEntity=Project::class, inversedBy="products")
     *
     * @var Collection
     */
    private $projects;

    /**
     * @ORM\OneToMany(targetEntity=ProductTypeProject::class, mappedBy="project")
     *
     * @var Collection
     */
    private $projectProductTypes;

    /**
     * @ORM\ManyToOne(targetEntity=ProductType::class, inversedBy="products")
     *
     * @var ProductType
     */
    private $productType;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->productCriterias    = new ArrayCollection;
        $this->projects            = new ArrayCollection;
        $this->projectProductTypes = new ArrayCollection;
    }

    public function getName()
    {
        return $this->name;
    }
    public function getId()
    {
        return $this->id;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
        }
        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
        }
        return $this;
    }

    public function getProjectProductType(): Collection
    {
        return $this->projectProductTypes;
    }

    public function addProjectProductType(ProductTypeProject $projectProductType): self
    {
        if (!$this->projectProductTypes->contains($projectProductType)) {
            $this->projectProductTypes[] = $projectProductType;
        }
        return $this;
    }

    public function removeProjectProductType(ProductTypeProject $projectProductType): self
    {
        if ($this->projectProductTypes->contains($projectProductType)) {
            $this->projectProductTypes->removeElement($projectProductType);
        }
        return $this;
    }

    public function getProductType(): ProductType
    {
        return $this->productType;
    }

    public function setProductType(ProductType $productType): void
    {
        $this->productType = $productType;
    }

    public function getProductCriterias(): Collection
    {
        return $this->productCriterias;
    }

    public function addProductCriteria(ProductCriteria $productCriteria): self
    {
        if (!$this->productCriterias->contains($productCriteria)) {
            $this->productCriterias[] = $productCriteria;
        }
        return $this;
    }

    public function removeProductCriteria(ProductCriteria $productCriteria): self
    {
        if ($this->productCriterias->contains($productCriteria)) {
            $this->productCriterias->removeElement($productCriteria);
        }
        return $this;
    }
}
