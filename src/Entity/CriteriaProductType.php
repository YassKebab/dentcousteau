<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="criteria_product_type")
 */
class CriteriaProductType
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isBeneficial;

    /**
     * @ORM\Column(type="float")
     */
    private $weight;

    /**
     * @ORM\ManyToOne(targetEntity=ProductType::class, inversedBy="productTypeCriterias")
     *
     * @var ProductType
     */
    private $productType;

    /**
     * @ORM\ManyToOne(targetEntity=Criteria::class, inversedBy="criteriaProductTypes")
     *
     * @var Criteria
     */
    private $criteria;

    public function getId()
    {
        return $this->id;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight): void
    {
        $this->weight = $weight;
    }

    public function getCriteria(): Criteria
    {
        return $this->criteria;
    }

    public function setCriteria(Criteria $criteria): void
    {
        $this->criteria = $criteria;
    }

    public function getProductType(): ProductType
    {
        return $this->productType;
    }

    public function setProductType(ProductType $productType): void
    {
        $this->productType = $productType;
    }

    public function getIsBeneficial(): bool
    {
        return $this->isBeneficial;
    }

    public function setIsBeneficial(bool $isBeneficial): void
    {
        $this->isBeneficial = $isBeneficial;
    }
}
