<?php


namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="productType")
 */
class ProductType
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $lib;

    /**
     * @ORM\OneToMany(targetEntity=ProductTypeProject::class, mappedBy="productType")
     *
     * @var Collection
     */
    private $productTypeProjects;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="productType")
     *
     * @var Collection
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity=CriteriaProductType::class, mappedBy="productType")
     *
     * @var Collection
     */
    private $productTypeCriterias;

    public function getLib()
    {
        return $this->lib;
    }
    public function getId()
    {
        return $this->id;
    }

    public function setLib($lib): void
    {
        $this->lib = $lib;
    }

    public function getProductTypeProjects(): Collection
    {
        return $this->productTypeProjects;
    }

    public function addProductTypeProject(ProductTypeProject $productTypeProject): self
    {
        if (!$this->productTypeProjects->contains($productTypeProject)) {
            $this->productTypeProjects[] = $productTypeProject;
        }
        return $this;
    }

    public function removeProductTypeProject(ProductTypeProject $productTypeProject): self
    {
        if ($this->productTypeProjects->contains($productTypeProject)) {
            $this->productTypeProjects->removeElement($productTypeProject);
        }
        return $this;
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }
        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }
        return $this;
    }

    public function getProductTypeCriterias(): Collection
    {
        return $this->productTypeCriterias;
    }

    public function addProductTypeCriterias(CriteriaProductType $productTypeCriterias): self
    {
        if (!$this->productTypeCriterias->contains($productTypeCriterias)) {
            $this->productTypeCriterias[] = $productTypeCriterias;
        }
        return $this;
    }

    public function removeProductTypeCriterias(CriteriaProductType $productTypeCriterias): self
    {
        if ($this->productTypeCriterias->contains($productTypeCriterias)) {
            $this->productTypeCriterias->removeElement($productTypeCriterias);
        }
        return $this;
    }
}
