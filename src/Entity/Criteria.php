<?php


namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="criteria")
 */
class Criteria
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * 
     * @Assert\NotBlank
     */
    private $lib;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $unit;

    /**
     * @ORM\OneToMany(targetEntity=ProductCriteria::class, mappedBy="criteria")
     *
     * @var Collection
     */
    private $criteriaProducts;


    /**
     * @ORM\OneToMany(targetEntity=CriteriaProductType::class, mappedBy="criteria")
     *
     * @var Collection
     */
    private $criteriaProductTypes;

    public function getId()
    {
        return $this->id;
    }

    public function getLib()
    {
        return $this->lib;
    }

    public function setLib($lib): void
    {
        $this->lib = $lib;
    }

    public function getUnit()
    {
        return $this->unit;
    }

    public function setUnit($unit): void
    {
        $this->unit = $unit;
    }

    public function getCriteriaProducts(): Collection
    {
        return $this->criteriaProducts;
    }

    public function addCriteriaProduct(ProductCriteria $criteriaProduct): self
    {
        if (!$this->criteriaProducts->contains($criteriaProduct)) {
            $this->criteriaProducts[] = $criteriaProduct;
        }
        return $this;
    }

    public function removeCriteriaProduct(ProductCriteria $criteriaProduct): self
    {
        if ($this->criteriaProducts->contains($criteriaProduct)) {
            $this->criteriaProducts->removeElement($criteriaProduct);
        }
        return $this;
    }

    public function getCriteriaProductTypes(): Collection
    {
        return $this->criteriaProductTypes;
    }

    public function addCriteriaProductTypes(CriteriaProductType $criteriaProductTypes): self
    {
        if (!$this->criteriaProductTypes->contains($criteriaProductTypes)) {
            $this->criteriaProductTypes[] = $criteriaProductTypes;
        }
        return $this;
    }

    public function removeCriteriaProductTypes(CriteriaProductType $criteriaProductTypes): self
    {
        if ($this->criteriaProductTypes->contains($criteriaProductTypes)) {
            $this->criteriaProductTypes->removeElement($criteriaProductTypes);
        }
        return $this;
    }
}
