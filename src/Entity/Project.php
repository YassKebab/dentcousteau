<?php


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="project")
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=ProductTypeProject::class, mappedBy="project")
     *
     * @var Collection
     */
    private $projectProductTypes;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class, mappedBy="projects")
     *
     * @var Collection
     */
    private $products;

    /**
     * Project constructor.
     * @param $id
     */
    public function __construct()
    {
        $this->projectProductTypes = new ArrayCollection;
        $this->products            = new ArrayCollection;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }
        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }
        return $this;
    }

    public function getProjectProductTypes(): Collection
    {
        return $this->projectProductTypes;
    }

    public function addProjectProductTypes(ProductTypeProject $projectProductTypes): self
    {
        if (!$this->projectProductTypes->contains($projectProductTypes)) {
            $this->projectProductTypes[] = $projectProductTypes;
        }
        return $this;
    }

    public function removeProjectProductTypes(ProductTypeProject $projectProductTypes): self
    {
        if ($this->projectProductTypes->contains($projectProductTypes)) {
            $this->projectProductTypes->removeElement($projectProductTypes);
        }
        return $this;
    }
}
