<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_criteria")
 */
class ProductCriteria
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $value;

    /**
     * @ORM\Column(type="float")
     */
    private $numericValue;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="productCriterias")
     *
     * @var Product
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=Criteria::class, inversedBy="criteriaProducts")
     *
     * @var Criteria
     */
    private $criteria;

    public function getId()
    {
        return $this->id;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value): void
    {
        $this->value = $value;
    }


    public function getCriteria(): Criteria
    {
        return $this->criteria;
    }

    public function setCriteria(Criteria $criteria): void
    {
        $this->criteria = $criteria;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getNumericValue()
    {
        return $this->numericValue;
    }

    /**
     * @param mixed $numericValue
     */
    public function setNumericValue($numericValue): void
    {
        $this->numericValue = $numericValue;
    }

}
