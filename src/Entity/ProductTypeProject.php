<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="project_product_type")
 */
class ProductTypeProject
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $nb;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="projectProductTypes")
     *
     * @var Project
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity=ProductType::class, inversedBy="productTypeProjects")
     *
     * @var ProductType
     */
    private $productType;

    public function getId()
    {
        return $this->id;
    }

    public function getProductType(): ProductType
    {
        return $this->productType;
    }

    public function setProductType(ProductType $productType): void
    {
        $this->productType = $productType;
    }

    public function getNb()
    {
        return $this->nb;
    }

    public function setNb($nb): void
    {
        $this->nb = $nb;
    }

    public function getProject(): Project
    {
        return $this->project;
    }

    public function setProject(Project $project): void
    {
        $this->project = $project;
    }


}
